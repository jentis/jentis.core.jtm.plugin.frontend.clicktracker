(!code:start)
function(jtsTracker,oGateConfig){

	this.oConf = oGateConfig;
	this.oTracker = jtsTracker;
	
	this.init = function(){
		this.oTracker.registerGateFunc("pageview",this.pageview,this);
	}
	
	this.pageview = function(){
	
		var self = this;

		document.addEventListener("DOMContentLoaded", function(event) { 
	
			var aMatches = document.querySelectorAll(self.oConf.selector);
			
			aMatches.forEach(function(oElement) {
				
				oElement.addEventListener("click", function(e) {
					
					window._jts.push({
						"track" : "event",
						"group" : self.oConf.event_group,
						"name" 	: self.oConf.event_name,
						"value" : this.getAttribute('href')
					});
					
				}, false);				
				
			});
	
		});

	}
	
	this.init();
}
(!code:end)